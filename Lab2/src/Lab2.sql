-- 1). Выведите название отдела и средний размер комиссионных сотрудников этого отдела. Если ни одному из сотрудников не выплачиваются комиссионные, необходимо вывести название отдела и 0.
SELECT
  D1.DEPARTMENT_NAME,
  avg(E1.COMMISSION_PCT)
FROM EMPLOYEES E1 RIGHT JOIN DEPARTMENTS D1 ON E1.DEPARTMENT_ID = D1.DEPARTMENT_ID
GROUP BY D1.DEPARTMENT_NAME;

-- 2). Создайте запрос, выводящий сумму заработной платы, среднюю заработную плату, минимальную и максимальную заработную плату для:
-- a.	Каждой должности, с указанием типа работы

SELECT
  J1.JOB_TITLE,
  SUM(E1.SALARY),
  AVG(E1.SALARY),
  MAX(E1.SALARY),
  MIN(E1.SALARY)
FROM EMPLOYEES E1 RIGHT JOIN JOBS J1 ON E1.JOB_ID = J1.JOB_ID
GROUP BY J1.JOB_TITLE;

-- b.	Каждого отдела, с указанием наименования отдела
SELECT
  D1.DEPARTMENT_NAME,
  SUM(E1.SALARY),
  AVG(E1.SALARY),
  MAX(E1.SALARY),
  MIN(E1.SALARY)
FROM EMPLOYEES E1 RIGHT JOIN DEPARTMENTS D1 ON E1.DEPARTMENT_ID = D1.DEPARTMENT_ID
GROUP BY D1.DEPARTMENT_NAME;

-- c.	Для каждого типа должностей сотрудников 30 отдела
SELECT
  J1.JOB_TITLE,
  SUM(E1.SALARY),
  AVG(E1.SALARY),
  MAX(E1.SALARY),
  MIN(E1.SALARY)
FROM EMPLOYEES E1
  RIGHT JOIN DEPARTMENTS D1 ON E1.DEPARTMENT_ID = D1.DEPARTMENT_ID
  RIGHT JOIN JOBS J1 ON E1.JOB_ID = J1.JOB_ID
WHERE D1.DEPARTMENT_ID = 30
GROUP BY J1.JOB_TITLE;

-- 3). Посчитайте количество сотрудников каждой должности в каждом отделе. В результатах запроса укажите название отдела и название должности. Результат запроса должен быть отсортирован сначала по названию отделов, потом по названию должностей.
SELECT
  D1.DEPARTMENT_NAME             "Отдел",
  J1.JOB_TITLE                   "Должность",
  COUNT(DISTINCT E1.EMPLOYEE_ID) "Количество"
FROM EMPLOYEES E1
  LEFT JOIN DEPARTMENTS D1 ON E1.DEPARTMENT_ID = D1.DEPARTMENT_ID
  RIGHT JOIN JOBS J1 ON E1.JOB_ID = J1.JOB_ID
GROUP BY D1.DEPARTMENT_NAME, J1.JOB_TITLE
ORDER BY D1.DEPARTMENT_NAME, J1.JOB_TITLE;

-- 4). Посчитайте разность между максимальной и минимальной зарплатой для:
-- a.	Каждой должности, в выводе укажите должность
SELECT
  J.JOB_TITLE,
  MAX(E.SALARY) - MIN(E.SALARY)
FROM EMPLOYEES E RIGHT JOIN JOBS J ON E.JOB_ID = J.JOB_ID
GROUP BY J.JOB_TITLE;

-- b.	Каждого отдела, в выводе указывайте название отдела
SELECT
  D.DEPARTMENT_NAME,
  MAX(E.SALARY) - MIN(E.SALARY)
FROM EMPLOYEES E INNER JOIN DEPARTMENTS D ON E.DEPARTMENT_ID = D.DEPARTMENT_ID
GROUP BY D.DEPARTMENT_NAME;

-- 5). Для каждого менеджера выведите его порядковый номер, имя, и наиболее высокооплачиваемого сотрудника этого менеджера. Сотрудники, чей менеджер не определен, в запросе не учитываются.

SELECT
  E2.MANAGER_ID,
  MAX(E1.SALARY)
FROM EMPLOYEES E1 INNER JOIN EMPLOYEES E2 ON E1.EMPLOYEE_ID = E2.MANAGER_ID
GROUP BY E2.MANAGER_ID;

-- 6). В предыдущий запрос добавьте условие: вывести только те строки, где
-- a.	Максимальная зарплата больше 8000
SELECT
  E2.MANAGER_ID,
  MAX(E1.SALARY)
FROM EMPLOYEES E1 INNER JOIN EMPLOYEES E2 ON E1.EMPLOYEE_ID = E2.MANAGER_ID
GROUP BY E2.MANAGER_ID
HAVING MAX(E1.SALARY) > 8000;

-- b.	Номер менеджера больше 7700
-- ????

-- 7). Напишите запрос, возвращающий фамилию сотрудников, их заработную плату и номер отдела для тех сотрудников, чья зарплата выше средней зарплаты в их отделе
SELECT
  E1.FIRST_NAME,
  E1.SALARY,
  E1.DEPARTMENT_ID
FROM EMPLOYEES E1
GROUP BY E1.FIRST_NAME, E1.SALARY, E1.DEPARTMENT_ID
HAVING E1.SALARY > (SELECT AVG(SALARY)
                    FROM EMPLOYEES E2
                    WHERE E2.DEPARTMENT_ID = E1.DEPARTMENT_ID);

-- 8). Выведите фамилию, номер департамента и должность для всех сотрудников, работающих в отделе, название которого содержит S.
SELECT
  E1.LAST_NAME,
  D1.DEPARTMENT_NAME,
  J1.JOB_TITLE
FROM EMPLOYEES E1
  RIGHT JOIN DEPARTMENTS D1 ON E1.DEPARTMENT_ID = D1.DEPARTMENT_ID
  RIGHT JOIN JOBS J1 ON E1.JOB_ID = J1.JOB_ID
WHERE D1.DEPARTMENT_NAME LIKE '%S%';

-- 9). Создайте запрос, который возвращает имя, должность и зарплату для всех сотрудников, чья зарплат выше средней, и чьим коллегой является сотрудник с именем, содержащим ‘T’.
SELECT
  E1.FIRST_NAME,
  J1.JOB_TITLE,
  E1.SALARY
FROM EMPLOYEES E1
  RIGHT JOIN JOBS J1 ON E1.JOB_ID = J1.JOB_ID
WHERE E1.SALARY > (SELECT avg(E2.SALARY)
                   FROM EMPLOYEES E2) AND (SELECT COUNT(*)
                                           FROM EMPLOYEES E3
                                           WHERE E1.EMPLOYEE_ID <>
                                                 E3.EMPLOYEE_ID, E3.DEPARTMENT_ID = E1.DEPARTMENT_ID AND E3.FIRST_NAME LIKE '%T%') > 0;

-- 10). Напишите запрос, который выводит фамилию, зарплату и комиссию тех сотрудников, чья зарплата совпадает с зарплатой хотя бы одного коллеги (то есть человека, работающего в том же департаменте), получающего комиссию.

SELECT
  E1.LAST_NAME,
  E1.SALARY,
  E1.COMMISSION_PCT
FROM EMPLOYEES E1
  INNER JOIN EMPLOYEES E2 ON E1.DEPARTMENT_ID = E2.DEPARTMENT_ID
WHERE E1.SALARY = E2.SALARY AND E2.DEPARTMENT_ID = E1.DEPARTMENT_ID
      AND E2.COMMISSION_PCT IS NOT NULL
      AND E2.EMPLOYEE_ID <> E1.EMPLOYEE_ID
GROUP BY E1.LAST_NAME, E1.SALARY, E1.COMMISSION_PCT
ORDER BY E1.LAST_NAME;
