-- 2). Выведите имена сотрудников, чей номер телефона
-- 1.	начинается на 590
-- 2.	содержит 21
-- 3.	содержит 21 во второй тройке цифр
-- 4.	телефон состоит из 4 групп цифр
SELECT FIRST_NAME
FROM EMPLOYEES
WHERE PHONE_NUMBER LIKE '590%';

SELECT FIRST_NAME
FROM EMPLOYEES
WHERE PHONE_NUMBER LIKE '%21%';

SELECT FIRST_NAME
FROM EMPLOYEES
WHERE PHONE_NUMBER LIKE '%.%21%.';

SELECT FIRST_NAME
FROM EMPLOYEES
WHERE PHONE_NUMBER LIKE '%.%.%.%';

-- 3). Выведите email и телефон сотрудников, в имени или фамилии которых присутствует буква S (заглавная).
SELECT EMAIL, PHONE_NUMBER
FROM EMPLOYEES
WHERE FIRST_NAME LIKE '%S%' OR LAST_NAME LIKE '%S%';

-- 4). Выведите email и телефон сотрудников, в имени или фамилии которых присутствует буква S и не присутствует буква s.
SELECT FIRST_NAME, LAST_NAME, EMAIL, PHONE_NUMBER
FROM EMPLOYEES
WHERE (FIRST_NAME LIKE '%S%' OR LAST_NAME LIKE '%S%') AND FIRST_NAME NOT LIKE '%s%' AND LAST_NAME NOT LIKE '%s%';

-- 5). Выведите сотрудников, чьим менеджером является Стивен (Steven). Отсортируйте их в алфавитном порядке по фамилии и имени.
SELECT E1.EMPLOYEE_ID, E1.FIRST_NAME, E2.LAST_NAME
FROM EMPLOYEES E1 INNER JOIN EMPLOYEES E2 ON E1.MANAGER_ID = E2.EMPLOYEE_ID
WHERE E2.FIRST_NAME = 'Steven'
ORDER BY E1.FIRST_NAME, E2.LAST_NAME;

-- 6). Создайте запрос, возвращающий фамилию, номер отдела, заработную плату и дату приема на работу сотрудника.
-- 1.Результат запроса отсортируйте по фамилиям
SELECT LAST_NAME, DEPARTMENT_ID, SALARY, HIRE_DATE
FROM EMPLOYEES
ORDER BY LAST_NAME;
-- 2.Результат запроса отсортируйте по датам так, чтобы принятые последними возглавляли список
SELECT LAST_NAME, DEPARTMENT_ID, SALARY, HIRE_DATE
FROM EMPLOYEES
ORDER BY HIRE_DATE DESC;
-- 3.Результат запроса отсортируйте по номерам отделов. Внутри отдела служащие должны перечисляться в порядке возрастания заработной платы
SELECT LAST_NAME, DEPARTMENT_ID, SALARY, HIRE_DATE
FROM EMPLOYEES
ORDER BY DEPARTMENT_ID, SALARY;

-- 7). Выполните следующие запросы и проанализируйте результат. Выедите ту же информацию для дат SYSDATE-6 месяцев, SYSDATE+6 месяцев. Объясните результат запросов.
SELECT SYSDATE, ROUND(SYSDATE, 'MONTH'), ROUND(SYSDATE, 'YEAR'), ROUND(ADD_MONTH(SYSDATE, -6), 'MONTH'),
    ROUND(ADD_MONTH(SYSDATE, -6), 'YEAR'), ROUND(ADD_MONTH(SYSDATE, +6), 'MONTH'), ROUND(ADD_MONTH(SYSDATE, +6), 'YEAR')
FROM DUAL;

SELECT SYSDATE, TRUNC(SYSDATE, 'MONTH'), TRUNC(SYSDATE, 'YEAR'), TRUNC(ADD_MONTH(SYSDATE, -6), 'MONTH'),
    TRUNC(ADD_MONTH(SYSDATE, -6), 'YEAR'), TRUNC(ADD_MONTH(SYSDATE, +6), 'MONTH'), TRUNC(ADD_MONTH(SYSDATE, +6), 'YEAR')
FROM DUAL;

-- 8). Выведите имена сотрудников, их должности и названия регионов, в которых они работают. Результат должен быть отсортирован по названию городов.
SELECT EMPLOYEES.FIRST_NAME, JOBS.JOB_TITLE, REGIONS.REGION_NAME
FROM EMPLOYEES
    INNER JOIN DEPARTMENTS ON EMPLOYEES.DEPARTMENT_ID = DEPARTMENTS.DEPARTMENT_ID
    INNER JOIN LOCATIONS ON DEPARTMENTS.LOCATION_ID = LOCATIONS.LOCATION_ID
    INNER JOIN COUNTRIES ON LOCATIONS.COUNTRY_ID = COUNTRIES.COUNTRY_ID
    INNER JOIN REGIONS ON COUNTRIES.REGION_ID = REGIONS.REGION_ID
    INNER JOIN JOBS ON EMPLOYEES.JOB_ID = JOBS.JOB_ID
ORDER BY LOCATIONS.CITY;

-- 9). Выведите имена сотрудников, их должности, процент  комиссионных, названия отделов и названия регионов для всех сотрудников, которые получают комиссионные. Процент комиссионных – это процент, который комиссионные составляют от зарплаты, озаглавьте этот столбец ‘comm %’
SELECT EMPLOYEES.FIRST_NAME, JOBS.JOB_TITLE, (EMPLOYEES.COMMISSION_PCT * 100) AS "comm%", DEPARTMENTS.DEPARTMENT_NAME,
    REGIONS.REGION_NAME
FROM EMPLOYEES
    INNER JOIN DEPARTMENTS ON EMPLOYEES.DEPARTMENT_ID = DEPARTMENTS.DEPARTMENT_ID
    INNER JOIN LOCATIONS ON DEPARTMENTS.LOCATION_ID = LOCATIONS.LOCATION_ID
    INNER JOIN COUNTRIES ON LOCATIONS.COUNTRY_ID = COUNTRIES.COUNTRY_ID
    INNER JOIN REGIONS ON COUNTRIES.REGION_ID = REGIONS.REGION_ID
    INNER JOIN JOBS ON EMPLOYEES.JOB_ID = JOBS.JOB_ID
WHERE EMPLOYEES.COMMISSION_PCT IS NOT NULL;

-- 10). Выведите должность сотрудника ‘KING’ и название отдела, в котором он работает.
SELECT JOBS.JOB_TITLE, DEPARTMENTS.DEPARTMENT_NAME
FROM EMPLOYEES
    INNER JOIN JOBS ON EMPLOYEES.JOB_ID = JOBS.JOB_ID
    INNER JOIN DEPARTMENTS ON EMPLOYEES.DEPARTMENT_ID = DEPARTMENTS.DEPARTMENT_ID
WHERE EMPLOYEES.LAST_NAME = 'King';

--  11). Выведите фамилию сотрудника (озаглавьте столбец ‘Фамилия сотрудника’),  и если известен менеджер, то фамилию менеджера (озаглавьте столбец ‘Фамилия менеджера’), иначе null.
SELECT E1.FIRST_NAME "Сотрудник", E2.FIRST_NAME "Менеджер"
FROM EMPLOYEES E1 LEFT JOIN EMPLOYEES E2 ON E1.MANAGER_ID = E2.EMPLOYEE_ID;

-- 12). Составьте запрос, который для каждого сотрудника выведет фамилию, номер отдела, в котором он работает, и фамилии всех сотрудников, которые работают в том же отделе (столбец озаглавьте ‘Коллеги’)

SELECT E1.LAST_NAME             "Сотрудник", (LISTAGG(E2.LAST_NAME, ', ')
WITHIN GROUP (
    ORDER BY E1.DEPARTMENT_ID)) "Коллеги"
FROM EMPLOYEES E1 RIGHT JOIN EMPLOYEES E2 ON E1.DEPARTMENT_ID = E2.DEPARTMENT_ID
WHERE E1.EMPLOYEE_ID <> E2.EMPLOYEE_ID
GROUP BY E1.LAST_NAME;
